addAction(INIT, function() {
    var $wrap = $('.wrap');
    var $contentSections = $('.content-section');
    var CSS_PROPERTY = 'margin-bottom';

    if ($contentSections.length <= 1) {
        return;
    }
    addAction(LAYOUT, function() {
        var headerHeight = $('.header-nav').height();
        $wrap.css(CSS_PROPERTY, headerHeight);
    });
});
