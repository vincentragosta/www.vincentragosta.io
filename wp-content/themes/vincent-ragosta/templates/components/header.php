<?php
use Backstage\SetDesign\NavMenu\NavMenuView;
?>

<header class="header-nav sticky-header" data-gtm="Header">
    <?php if (has_nav_menu('primary_navigation')): ?>
        <div class="header-nav__menu">
            <?= NavMenuView::createResponsive(); ?>
        </div>
    <?php endif; ?>
</header>
