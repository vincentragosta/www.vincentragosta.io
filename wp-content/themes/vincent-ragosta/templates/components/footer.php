<?php

use Backstage\SetDesign\Modal\ModalView;
use Backstage\SetDesign\SocialIcons\SocialIconsView;
use Orchestrator\GlobalOptions;
?>
<div class="footer-nav">
    <div class="footer-nav__copyright d-none d-md-block">
        <p>&copy; <?= new \Backstage\Support\DateTime('now', 'Y'); ?> All Rights Reserved.</p>
    </div>
    <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
        <div class="footer-nav__social-icons">
            <?= SocialIconsView::create($social_icons); ?>
        </div>
    <?php endif; ?>
</div>
<?= ModalView::unloadAll(); ?>
