<?php

namespace ChildTheme\Project;

use Backstage\Models\PostBase;

/**
 * Class Project
 * @package ChildTheme\Project
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $website_url
 */
class Project extends PostBase
{
    const POST_TYPE = 'project';
}
