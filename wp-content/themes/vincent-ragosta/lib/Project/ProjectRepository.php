<?php

namespace ChildTheme\Project;

use Backstage\Repositories\PostRepository;

/**
 * Class ProjectRepository
 * @package ChildTheme\Project
 * @author Vincent Ragosta <vragosta@situationinteracitive.com>
 * @version 1.0
 */
class ProjectRepository extends PostRepository
{
    protected $model_class = Project::class;
}
