<?php

namespace ChildTheme\Components\PostActionCard;

use Backstage\Models\PostBase;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\View\Component;
use Backstage\View\Link;
use \WP_Image;

/**
 * Class PostActionCardView
 * @package ChildTheme\Components\PostActionCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $title
 * @property WP_Image|bool $image
 * @property string $content
 * @property array $link
 */
class PostActionCardView extends Component
{
    const MODAL_ID = 'post-action-card-modal';
    const IMAGE_SIZE = 768;

    protected $name = 'post-action-card';
    protected static $default_properties = [
        'title' => '',
        'image' => false,
        'content' => '',
        'link' => []
    ];

    public function __construct(PostBase $Post)
    {
        ModalView::load(static::MODAL_ID, 'box');
        parent::__construct([
            'title' => $Post->title(),
            'image' => $Post->featuredImage(),
            'content' => $Post->content(false),
            'link' => [
                'title' => 'See Website',
                'url' => $Post->website_url,
                'target' => '_blank'
            ]
        ]);
        if ($this->image instanceof WP_Image) {
            $this->image->width(static::IMAGE_SIZE)->height(static::IMAGE_SIZE);
        }
        $this->elementAttributes([
            'class' => 'js-modal-builder',
            'data-modal-target' => '#' . static::MODAL_ID
        ]);
    }
}
