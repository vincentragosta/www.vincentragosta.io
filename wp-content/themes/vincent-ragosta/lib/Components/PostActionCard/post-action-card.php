<?php
/**
 * Expected:
 * @var string $title
 * @var WP_Image|bool $image
 * @var string $content
 * @var array $link
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use Backstage\View\Link;

if (!$image instanceof WP_Image || empty($title)) {
    return '';
}
?>

<div <?= Util::componentAttributes('post-action-card', $class_modifiers, $element_attributes); ?>>
    <div class="post-action-card__image-container">
        <?= $image; ?>
        <div class="post-action-card__overlay">
            <?= new IconView(['icon_name' => 'plus', 'style' => 'contrast']); ?>
        </div>
    </div>
    <div class="post-action-card__container">
        <strong class="post-action-card__title"><?= $title; ?></strong>
        <?php if (!empty($content)): ?>
            <div class="post-action-card__content">
                <?= $content; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($link['url'])): ?>
            <?= Link::createFromField($link)->class('post-action-card__link'); ?>
        <?php endif; ?>
    </div>
</div>
