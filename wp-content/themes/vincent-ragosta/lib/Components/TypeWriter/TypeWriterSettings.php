<?php

namespace ChildTheme\Components\TypeWriter;

use Backstage\Models\ObjectBase;

/**
 * Class TypeWriterSettings
 * @package ChildTheme\Components\TypeWriter
 * @author Vincent Ragosta <vragosta @situationinteractive.com>
 * @version 1.0
 *
 * @property string $text
 */
class TypeWriterSettings extends ObjectBase
{
    const DELIMITER = ',';
    const PRE_ERASE_DELAY = 1000;
    const NO_REPEAT = 0;

    protected $text;

    public function __construct(string $text)
    {
        parent::__construct(compact('text'));
    }

    protected function hasDelimiter()
    {
        return strpos($this->text, static::DELIMITER) > 0;
    }

    public function getSettings()
    {
        return $this->hasDelimiter() ? $this->getRepeatProperties() : $this->getDefaultProperties();
    }

    protected function getRepeatProperties()
    {
        return [
            ':text' => json_encode(explode(static::DELIMITER, $this->text)),
            ':pre-erase-delay' => static::PRE_ERASE_DELAY
        ];
    }

    protected function getDefaultProperties()
    {
        return [
            ':text' => json_encode($this->text),
            ':repeat' => static::NO_REPEAT
        ];
    }
}
