<?php

namespace ChildTheme\Components\TypeWriter;

use Backstage\VcLibrary\Support\Component;
use Backstage\View\Element;

/**
 * Class TypeWriter
 * @package ChildTheme\Components\TypeWriter
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class TypeWriter extends Component
{
    const NAME = 'TypeWriter';
    const TAG = 'type_writer';

    protected $component_config = [
        'description' => 'Add typewriter effect to text.',
        'icon' => 'icon-wpb-atm',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Text',
                'param_name' => 'content',
                'description' => 'Set the heading text.',
                'group' => 'General',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        return Element::create('div',
            Element::create('vue-typer', '',
                (new TypeWriterSettings($atts['content']))->getSettings()
            ),
            ['id' => 'vue-typer']
        );
    }
}
