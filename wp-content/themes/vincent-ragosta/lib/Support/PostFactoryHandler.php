<?php

namespace ChildTheme\Support;

use Backstage\Models\PostFactory;
use ChildTheme\Project\Project;

/**
 * Class PostFactoryHandler
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @versio 1.0
 */
class PostFactoryHandler
{
    protected static $model_classes = [
        Project::class
    ];

    public function __construct()
    {
        foreach (static::$model_classes as $class) {
            PostFactory::registerPostModel($class);
        }
    }
}
