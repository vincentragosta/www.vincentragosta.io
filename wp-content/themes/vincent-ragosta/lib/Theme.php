<?php

namespace ChildTheme;

use ChildTheme\Components;
use ChildTheme\Support;
use Orchestrator\Theme as ThemeBase;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const REMOVE_DEFAULT_POST_TYPE = true;

    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
        'video-producer'
    ];

    const EXTENSIONS = [
        Components\TypeWriter\TypeWriter::class,
        Components\PostActionCard\PostActionCard::class,
        Support\PostFactoryHandler::class
    ];

    /**
     * Add theme-specific hooks
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Roboto+Mono&display=swap');
        wp_enqueue_script('vue', 'https://cdn.jsdelivr.net/npm/vue' . (defined('ENVIRONMENT') && ENVIRONMENT == 'local' ? '/dist/vue.js' : ''), false, null, true);
        wp_enqueue_script('vue-typer', 'https://unpkg.com/vue-typer/dist/vue-typer.min.js', ['vue'], null, true);
    }
}
