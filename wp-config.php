<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         'YOR*`m1HU@db^32Z[)+*+2nURJ+Q*kvWR+j6l{)I#f@,1r!L;JH;Qa%lg}cx)^B@');
define('SECURE_AUTH_KEY',  '6X8pD8q]1|`0R*cKwWRFF!Nxo<_+CZnYcwlud i@|AXaZWjw(A$+s)|9%}<:Ty |');
define('LOGGED_IN_KEY',    'l8)K!XaJm|g|d(emc}q;I$y+&OBjQfeC|bf}Wumm*5W(=uqM<kF^7,)&HupRV<]7');
define('NONCE_KEY',        '`Q88&Lpx6paS8vj@QZVo9&!-<Gwp=7A+BmpPG}Yg+e OrrEQI9%|yyRSACiyJJio');
define('AUTH_SALT',        '>`KU(c5E*us-<+W`x2[[_3)&~CsLaTzxW&t0b+{P_I3,C_} {zHKvI^`+*=W|+ O');
define('SECURE_AUTH_SALT', '.pIbW;3J-]VLXm-Z,QJ$@;?Y`t#ZO{!{V$w}i6Rsulw @hH8o((a0YZ$KaedvBNu');
define('LOGGED_IN_SALT',   '=t>YQX, [T*,rNC.6T/Q:Y<$M=sw]XgD$owC%p5*3Mh^s]3|]ogaE!0DkeYM|Io}');
define('NONCE_SALT',       '>L)3)WooTigE9(+tBfvjgv^gM+`J@5>XBDL0c7-g-(`:0>.WYU&;byjhvbk!]?^D');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
